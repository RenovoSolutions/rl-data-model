A tool for representing REST APIs as an observable data service

## Installation

Requires https://gitlab.com/RenovoSolutions/rl-http:
```
npm install rl-http
```
Install rl-data-model from npm:
```
npm install rl-data-model
```
Configure with Angular-CLI and SystemJs:
```
/** Map relative paths to URLs. */
const map: any = {
    'rl-data-model': 'vendor/rl-data-model'
};

/** User packages configuration. */
const packages: any = {
    'rl-data-model': {
        main: 'index.js'
    }
};
```
Modify angular-cli-build.js by adding this line to vendorNpmFiles:
```
'rl-data-model/**/*.+(js|js.map)'
```

## Usage
This library provides base classes that can be inherited to provide functionality for representing a rest API as an observable stream. 
Singleton:
```
import { SingletonModel, CollectionModel } from 'rl-data-model';
import { HttpUtility } from 'rl-http';

export class MySingletonModel extends SingletonModel<MyType> {
	constructor(http: HttpUtility) {
		super('api/mySingletonModel', http);
	}
}

export class MyCollectionModel extends CollectionModel<MyType> {
	constructor(http: HttpUtility) {
		super('api/myCollectionModel', http);
	}
}

// using the model classes
mySingletonModel.subscribe(item => console.log(item));	// makes a GET request to /api/mySingletonModel on the first subscription
mySingletonModel.update(updatedItem).subscribe();		// makes a PUT request to /api/mySingletonModel and then updates the stream

myCollectionModel.subscribe(list => console.log(list));	// makes a GET request to /api/myCollectionModel on the first subscription
myCollectionModel.add(item);							// makes a POST request to /api/myCollectionModel and then updates the stream
myCollectionModel.update(item, list[2]);				// makes a PUT request to /api/myCollectionModel/{item.id} and then updates the stream
myCollectionModel.delete(list[2]);						// makes a DELETE request to /api/myCollectionModel/{list[2].id} and then updates the stream

// to get the underlying observable
mySingletonModel.asObservable();                        // makes a GET request to /api/mySingletonModel on the first call
myCollectionModel.asObservable();                       // makes a GET request to /api/myCollectionModel on the first call
```
Since the base implementation is inherited, it's easy to hook in and customize the behavior:
```
export class MyReactiveModel extends SingletonModel<MyType> {
	constructor(http: HttpUtility, private otherService: MyOtherService) {
		super('', http);
	}
	
	load(): void {
		this.otherService.selectedItem$.subscribe(item => {
			this.url = `api/reactive/${item.id}`;
			super.load();
		});
	}
}
```
This will reload the model every time the 'otherService' selected item stream fires an event.

It's also possible to inherit the base model directly to get the observable management without wiring up any http hooks. This is useful if you want a model class that doesn't talk to the API at all, or you have a lot of custom logic.
```
import { BaseModel } from 'rl-data-model';

export class MyCustomModel extends BaseModel<MyType> {
	constructor() {
		super(null, null);
	}
	
	select(item: MyType): void {
	    this.dataStream$.next(item);
	}
}

// usage
myCustomModel.subscribe();          // subscribes to the model stream
myCustomModel.asObservable();       // gets the underlying observable
myCustomModel.select(item);         // custom functionality implemented by the subclass
```
dataStream$ is a protected property used by the model classes to control the stream.

## Tests
This project uses systemjs and karma to run unit tests.
```
npm install
npm run build
npm test
```
Alternately:
```
npm install
npm run build-test
```
If you encounter an issue and want to debug it:
```
npm run test.debug
```
or
```
npm run build-test.watch
```
(Runs tsc in watch mode and concurrently runs the test debugger)