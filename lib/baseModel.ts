import { Observable, BehaviorSubject, Subscription } from 'rxjs';

import { HttpUtility } from 'rl-http';
import { INextHandler, IErrorHandler, ICompletionHandler } from './modelTypes';

export class BaseModel<T> {
	protected dataStream$: BehaviorSubject<T>;

	url: string;
	http: HttpUtility;

	constructor(url: string, http: HttpUtility) {
		this.url = url;
		this.http = http;
	}

	subscribe(onNext?: INextHandler<T>, onError?: IErrorHandler, onCompletion?: ICompletionHandler): Subscription {
		if (!this.dataStream$) {
			this.init();
			this.load();
		}

		return this.dataStream$.subscribe(onNext, onError, onCompletion);
	}

	asObservable(): Observable<T> {
		if (!this.dataStream$) {
			this.init();
			this.load();
		}

		return this.dataStream$.asObservable();
	}

	load(): Observable<T> {
		// make a request then push to stream
		return Observable.empty<T>();
	}

	protected init(): void {
		this.dataStream$ = new BehaviorSubject<T>(null);
	}
}
