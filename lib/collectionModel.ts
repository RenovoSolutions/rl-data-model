import { Observable } from 'rxjs';
import { map, without } from 'lodash';

import { HttpUtility } from 'rl-http';
import { BaseModel } from './baseModel';

export interface IdentityModel {
	id: number;
}

export class CollectionModel<T extends IdentityModel> extends BaseModel<T[]> {
	transform: { (data: any): T };

	constructor(url: string, http: HttpUtility) {
		super(url, http);
		this.transform = data => data;
	}

	add(newModel: T): Observable<T> {
		const request = this.http.post<T>(this.url, newModel).map(this.transform).share();
		request.subscribe(model =>
			this.dataStream$.next([...(this.dataStream$.getValue()), model])
		);
		return request;
	}

	update(newModel: T, oldModel: T): Observable<T> {
		const putUrl = this.url + '/' + oldModel.id;
		const request = this.http.put<T>(putUrl, newModel).map(this.transform).share();
		request.subscribe(updatedModel => {
			const updatedList = map(this.dataStream$.getValue(), model => {
				if (model.id === oldModel.id) {
					return updatedModel || newModel;
				}
				return model;
			});
			this.dataStream$.next(updatedList);
		});
		return request;
	}

	remove(model: T): Observable<void> {
		const deleteUrl = this.url + '/' + model.id;
		const request = this.http.delete(deleteUrl).share();
		request.subscribe(() =>
			this.dataStream$.next(without<T>(this.dataStream$.getValue(), model))
		);
		return request;
	}

	load(): Observable<T[]> {
		const request = this.http.get<T[]>(this.url).map(modelList => map(modelList, this.transform)).share();
		request.subscribe(modelList => this.dataStream$.next(modelList));
		return request;
	}
}
