export interface INextHandler<T> {
	(value: T): void;
}

export interface IErrorHandler {
	(error: any): void;
}

export interface ICompletionHandler {
	(): void;
}
