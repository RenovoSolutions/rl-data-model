import { rlFakeAsync, mock, IMockedRequest } from 'rl-async-testing';

import { SingletonModel } from './singletonModel';

interface IHttpMock {
	get?: IMockedRequest<number>;
	put?: IMockedRequest<number>;
}

describe('SingletonModel', () => {
	let model: SingletonModel<number>;
	let http: IHttpMock;

	beforeEach(() => {
		http = {
			get: mock.request(),
			put: mock.request(),
		};
		model = new SingletonModel<number>('/test', <any>http);

		expect(model.url).to.equal('/test');
	});

	it('should make an http request to get the item, then push it to the stream', rlFakeAsync(() => {
		let item;
		const itemFromServer = 11;
		http.get = mock.request(itemFromServer);

		model.subscribe(data => item = data);

		expect(item).to.be.null;
		sinon.assert.calledOnce(http.get);

		http.get.flush();

		expect(item).to.equal(itemFromServer);
	}));

	it('should return the http request', rlFakeAsync(() => {
		const itemFromServer = 11;
		let itemReturnedFromAction;
		const completeSpy = sinon.spy();
		http.get = mock.request(itemFromServer);
		(model as any).init();

		model.load().subscribe(data => itemReturnedFromAction = data, null, completeSpy);

		expect(itemReturnedFromAction).to.not.exist;
		sinon.assert.calledOnce(http.get);

		http.get.flush();

		expect(itemReturnedFromAction).to.equal(itemFromServer);
		sinon.assert.calledOnce(completeSpy);
	}));

	it('should make an http request to update the item, then push the updated item to the stream', rlFakeAsync(() => {
		let itemToSendForUpdate = 21;
		let itemFromDataStream;
		let itemReturnedFromAction;
		http.put = mock.request((url, model) => model);

		model.subscribe(data => itemFromDataStream = data);
		model.update(itemToSendForUpdate).subscribe(data => itemReturnedFromAction = data);

		expect(itemFromDataStream).to.be.null;
		expect(itemReturnedFromAction).to.not.exist;
		sinon.assert.calledOnce(http.put);

		http.get.flush();
		http.put.flush();

		expect(itemFromDataStream).to.equal(itemToSendForUpdate);
		expect(itemReturnedFromAction).to.equal(itemToSendForUpdate);
	}));

	it('should map the item using the specified transform with a load action', rlFakeAsync(() => {
		const itemFromServer = 11;
		let item;
		const expectedResult = 22;
		http.get = mock.request(itemFromServer);
		model.transform = x => x * 2;

		model.subscribe(data => item = data);

		expect(item).to.not.exist;
		sinon.assert.calledOnce(http.get);

		http.get.flush();

		expect(item).to.equal(expectedResult);
	}));

	it('should map the item using the specified transform with an update action', rlFakeAsync(() => {
		let itemToSendForUpdate = 11;
		let itemFromDataStream;
		let itemReturnedFromAction;
		const expectedResult = 22;
		http.put = mock.request((url, model) => model);
		model.transform = x => x * 2;

		model.subscribe(data => itemFromDataStream = data);
		model.update(itemToSendForUpdate).subscribe(data => itemReturnedFromAction = data);

		http.get.flush();
		http.put.flush();

		expect(itemFromDataStream).to.equal(expectedResult);
		expect(itemReturnedFromAction).to.equal(expectedResult);
	}));
});
